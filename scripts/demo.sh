#!/bin/bash

rosservice call /kcl_rosplan/update_knowledge_base "update_type: 0
knowledge:
  knowledge_type: 0
  instance_type: 'ardrone'
  instance_name: 'ar1'
  attribute_name: ''
  function_value: 0.0";

rosservice call /kcl_rosplan/update_knowledge_base "update_type: 0
knowledge:
  knowledge_type: 1
  instance_type: ''
  instance_name: ''
  attribute_name: 'on-ground'
  values:
  - {key: 'd', value: 'ar1'}
  function_value: 0.0"

rosservice call /kcl_rosplan/update_knowledge_base "update_type: 1
knowledge:
  knowledge_type: 1
  instance_type: ''
  instance_name: ''
  attribute_name: 'airborne'
  values:
  - {key: 'd', value: 'ar1'}
  function_value: 0.0"
