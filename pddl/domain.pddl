(define (domain ardrone)

(:requirements :strips :typing :fluents :disjunctive-preconditions :durative-actions)

(:types    
    ardrone
)

(:predicates
    (airborne ?d - ardrone)
    (on-ground ?d - ardrone)
)


(:durative-action takeoff
    :parameters (?d - ardrone)
    :duration ( = ?duration 3)
    :condition (and
        (at start (on-ground ?d)))
    :effect (and
        (at start (not (on-ground ?d)))
        (at end (airborne ?d)))
)

(:durative-action land
    :parameters (?d - ardrone)
    :duration ( = ?duration 3)
    :condition (and
        (at start (airborne ?d)))
    :effect (and
        (at start (not (airborne ?d)))
        (at end (on-ground ?d)))
)


)
