#!/usr/bin/env python

import rospy
from std_msgs.msg import Empty
import rosplan_dispatch_msgs.msg  
import rosplan_knowledge_msgs.srv
from diagnostic_msgs.msg import KeyValue

rospy.init_node('rpadrone', anonymous=True)

action_feedback_pub = rospy.Publisher('/kcl_rosplan/action_feedback', rosplan_dispatch_msgs.msg.ActionFeedback, queue_size=10)

takeoff_pub = rospy.Publisher('/ardrone/takeoff', Empty, queue_size=10)
land_pub = rospy.Publisher('/ardrone/land', Empty, queue_size=10)


rospy.wait_for_service('/kcl_rosplan/update_knowledge_base')
update_kb = rospy.ServiceProxy('/kcl_rosplan/update_knowledge_base', rosplan_knowledge_msgs.srv.KnowledgeUpdateService)

ar1 = KeyValue()
ar1.key = 'd'
ar1.value = 'ar1'

update_kb_req_g = rosplan_knowledge_msgs.srv.KnowledgeUpdateServiceRequest()
update_kb_req_g.update_type = update_kb_req_g.ADD_KNOWLEDGE 
update_kb_req_g.knowledge.knowledge_type = 1
update_kb_req_g.knowledge.attribute_name = "on-ground"
update_kb_req_g.knowledge.values = [ar1]

update_kb_req_a = rosplan_knowledge_msgs.srv.KnowledgeUpdateServiceRequest()
update_kb_req_a.update_type = update_kb_req_a.ADD_KNOWLEDGE 
update_kb_req_a.knowledge.knowledge_type = 1
update_kb_req_a.knowledge.attribute_name = "airborne"
update_kb_req_a.knowledge.values = [ar1]


def callback(msg):
	fb = rosplan_dispatch_msgs.msg.ActionFeedback()
	fb.action_id = msg.action_id
	fb.status = "action enabled"
	action_feedback_pub.publish(fb)

	if msg.name == "takeoff":
		e = Empty()
		takeoff_pub.publish(e)

		update_kb_req_g.update_type = update_kb_req_g.REMOVE_KNOWLEDGE 
		update_kb(update_kb_req_g)

		rospy.sleep(3);

		update_kb_req_a.update_type = update_kb_req_a.ADD_KNOWLEDGE 
		update_kb(update_kb_req_a)


		

	if msg.name == "land":
		e = Empty()
		land_pub.publish(e)
		
		update_kb_req_a.update_type = update_kb_req_g.REMOVE_KNOWLEDGE 
		update_kb(update_kb_req_a)

		rospy.sleep(3);

		update_kb_req_g.update_type = update_kb_req_a.ADD_KNOWLEDGE 
		update_kb(update_kb_req_g)



	fb = rosplan_dispatch_msgs.msg.ActionFeedback()
	fb.action_id = msg.action_id
	fb.status = "action achieved"
	action_feedback_pub.publish(fb)


rospy.Subscriber("/kcl_rosplan/action_dispatch", rosplan_dispatch_msgs.msg.ActionDispatch, callback)
rospy.spin()