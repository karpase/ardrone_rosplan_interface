import shapely
import shapely.wkt
import shapely.geometry
import shapely.geometry.polygon
import sys, random, math

EPSILON = 7.0
NUMNODES = 5000


def step_from_to(p1,p2):
    if p1.distance(p2) < EPSILON:
        return p2
    else:
    	l = shapely.geometry.LineString([p1,p2])
        return l.interpolate(EPSILON)

def frange(x, y, jump):
  while x < y:
    yield x
    x += jump



class Map:
	obstacles = []
	waypoints = {}

	def __init__(self, obstacles = [], filename=''):		
		self.add_obstacles(obstacles)
		if filename != '':
			self.add_obstacles_from_file(filename)

	def add_obstacles(self, obstacles):
		self.obstacles += obstacles

	def add_obstacles_from_file(self, filename):
		f = open(filename, 'r')
		for line in f:
			p = shapely.wkt.loads(line)
			self.obstacles.append(p)
		f.close()
		
	def dumps(self):
		for o in self.obstacles:
			print o.wkt

	def write_to_file(self, filename):
		f = open(filename, 'w')
		for o in self.obstacles:
			f.write(o.wkt + '\n')
		f.close()

	def is_straight_line_clear(self, start, end):
		l = shapely.geometry.LineString([start,end]) 	
		collision = False				
		for o in self.obstacles:
			if l.intersects(o):
				return False
		return True

	def add_waypoint(self, name, x, y):
		self.waypoints[name] = shapely.geometry.point.Point(x,y)

	def get_waypoint(self, name):
		return self.waypoints.get(name, None)

	def get_path(self, start, end):
		if self.is_straight_line_clear(start, end):
			return [start, end]

		c = shapely.geometry.collection.GeometryCollection(self.obstacles + [start, end])
		(minx, miny, maxx, maxy) = c.bounds
		
		# no clear straight line, use RRT
		nodes = {(start.x, start.y): None}
		for i in xrange(NUMNODES):
			rand = shapely.geometry.point.Point(random.uniform(minx, maxx), random.uniform(miny, maxy))
			nn = None
			nndist = float("inf")
			for p in nodes:	
				pp = shapely.geometry.point.Point(p[0], p[1])
				if nn is None or pp.distance(rand) < nndist:
					nn = pp
					nndist = nn.distance(rand)
			newnode = step_from_to(nn,rand)
			nodes[(newnode.x, newnode.y)] = nn
			if self.is_straight_line_clear(newnode, end):				
				path = [end]
				n = (newnode.x, newnode.y)
				while nodes[n] is not None:
					path.append(shapely.geometry.point.Point(n[0], n[1]))
					n = (nodes[n].x, nodes[n].y)
				path.append(start)
				path.reverse()
				return path
 				

